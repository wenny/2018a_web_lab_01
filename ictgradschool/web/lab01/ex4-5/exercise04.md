Exercise 04
===========
WENNY

Welcome to Markdown!

Markdown is a text-to-HTML conversion tool for web writers. 
Markdown allows you to write using an easy-to-read, easy-to-write 
plain text format, then convert it to structurally valid HTML.

Favorite Things
===============

These are a few of my favorite things

1.Raindrops on roses,
2.whiskers on kittens, 
3.bright copper kettles and warm woolen mittens.
4.Brown paper packages tied up with strings, 
5.cream-colored ponies and crisp apple strudels
6.Doorbells and sleigh bells, 
7.schnitzel with noodles, 
8.wild geese that fly with the moon on their wings.
9.Girls in white dresses with blue satin sashes,
10. snowflakes that stay on my nose and eyelashes, silver-white winters that melt into springs.

